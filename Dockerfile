FROM bitnami/jenkins
LABEL maintainer "Bitnami <containers@bitnami.com>"

## Change user to perform privileged actions
USER 0
## Install 'vim'
RUN install_packages vim

RUN apt-get update \
    && apt-get -y install ca-certificates \
    && apt-get -y  install curl \
    && apt-get -y  install gnupg \
    && apt-get -y  install lsb-release \
    && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
    && echo   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" |  tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt-get update \
    && apt-get -y install docker-ce \
    && apt-get -y install docker-ce-cli \
    && apt-get -y install containerd.io \
    && apt-get -y install docker-compose-plugin
## Revert to the original non-root user
